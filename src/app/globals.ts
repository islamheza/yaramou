import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
@Injectable()
export class Globals {
  WEBAPI = environment.WEBAPI;
  UPLOADS = environment.UPLOADS;

  getUrl = function(url) {
    return this.WEBAPI + this[url];
  };

  URL_CITIES_LOOKUP = "cities";
  URL_FORM_ADD = "information";
}
