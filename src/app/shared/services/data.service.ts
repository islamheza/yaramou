import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Globals } from "@zahran/globals";

@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient, private globals: Globals) {}
  getCities() {
    return this.http.get(this.globals.getUrl("URL_CITIES_LOOKUP"));
  }
  addEntry(formData) {
    return this.http.post(this.globals.getUrl("URL_FORM_ADD") , formData );
  }
}
