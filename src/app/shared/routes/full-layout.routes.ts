import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: '',
    loadChildren: () => import('@zahran/form-tab/form-tab.module').then(m => m.FormTabModule)
  },
];
