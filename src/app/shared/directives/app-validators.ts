import { FormGroup, FormControl } from "@angular/forms";

export function emailValidator(control: FormControl): {[key: string]: any} {
    let emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (!control.value || !emailRegexp.test(control.value.toLowerCase())) {
        return {invalidEmail: true};
    }
}

export function birthdateValidator(control: FormControl): {[key: string]: any} {
        if(control.value == null){
            return {invalidBirthdate: false};
    }
    let dateRegexp = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
    if (control.value && !dateRegexp.test(control.value)) {
        return {invalidBirthdate: true};
    }
}

export function passwordValidator(control: FormControl): {[key: string]: any} {
    var password =control.value;
    var flagForLength = password.length >= 6;
    var flagForUpper = /[A-Z]+/.test(password);
    var flagForDigit = /[0-9]+/.test(password);
    var flagForSpecial = /[!@#$%^&*()]+/.test(password);
    if(!flagForLength || !flagForUpper || !flagForDigit || !flagForSpecial){
      return {invalidPassword: true};
    }
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
export function nameValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    if(control.value == null){
      return {invalidName: false};
    }
    let matches = control.value.match(/\d+/g);


    if (matches != null) {
        return {invalidName: true};
       }
}
export function phoneValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    var flagForPhone = /^0[1][0-9]{9,10}$/.test(control.value);
    if (!(isNumeric(control.value)) || !flagForPhone  ) {
        return {invalidPhone: true};
       }
}
export function ageValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    if (!(isNumeric(control.value))) {
        return {invalidAge: true};
       }
}
export function govIdValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    if (!(isNumeric(control.value)) || control.value.length != 14  ) {
        return {invalidGovId: true};
       }
}

export function checkInValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    if(control.value == null ){
      return {invalidCheckin: true};
    }
}

export function checkOutValidator(control: FormControl): {[key: string]: any} {
    // Write Logic to Validate that Incoming control value have no numbers
    if(control.value == null){
      return {invalidCheckOut: true};
    }
}



export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        const password = group.controls[passwordKey];
        const passwordConfirmation = group.controls[passwordConfirmationKey];
        if (password.value !== passwordConfirmation.value) {
            return passwordConfirmation.setErrors({mismatchedPasswords: true});
        }
    };






}
