import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutFullComponent } from './layouts/layout-full/layout-full.component';
import { Full_ROUTES } from "./shared/routes/full-layout.routes";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full',
  },
  { path: '', component: LayoutFullComponent, data: { title: 'full Views' }, children: Full_ROUTES, canActivate: [],resolve: { } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
