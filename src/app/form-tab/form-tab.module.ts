import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormTabComponent } from "./form-tab.component";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DresscodeComponent } from './dresscode/dresscode.component';

const routes: Routes = [
  { path: "", component: FormTabComponent, pathMatch: "full" },
  { path: "dress-code", component: DresscodeComponent, pathMatch: "full" },

];

@NgModule({
  declarations: [FormTabComponent, DresscodeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
  ]

})

export class FormTabModule { }
