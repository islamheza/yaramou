import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray,
  Form
} from "@angular/forms";
import {
  emailValidator,
  checkOutValidator,
  checkInValidator,
  // birthdateValidator
} from "@zahran/shared/directives/app-validators";
import { DataService } from "@zahran/shared/services/data.service";
//import { error } from "console";
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';

@Component({
  selector: "app-form-tab",
  templateUrl: "./form-tab.component.html",
  styleUrls: ["./form-tab.component.scss"]
})
export class FormTabComponent implements OnInit {
  form: FormGroup;
  loading = false;
  formSubmitted = false;
  formFinish = false;
  formDone = false;
  formError = false;
  mobileExists = false;
  districts: any;
  errortype: any;
  choosenDays: string;
  count = 0;
  checkInDate;
  checkOutDate;
  incorrectDates = true;
  formIsShown = false;

  constructor(public formBuilder: FormBuilder, private dataService: DataService) {
  }
  ngOnInit(): void {

    //const home = document.querySelector<HTMLElement>('.formTab');
    //home.style.display = "none";

    this.form = this.formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ["", Validators.compose([Validators.required, emailValidator])],
      isGuest: [false],
      checkIn: ["", Validators.compose([Validators.required, checkInValidator])],
      checkOut: ["", Validators.compose([Validators.required, checkOutValidator])],
      rooms: ["1"],
      guests: this.formBuilder.array([]),
      message: [""],
      mobile:[null , Validators.required]
    });
  }

  createForm() {
    return this.formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ["", Validators.compose([])],
      checkIn: ["1989-05-24"],
      checkOut: ["1989-05-24"],
      rooms: [0],
      isGuest: [true],
      message: [""],
    });
  }

  addForm() {
    this.count++;
    let formLength = document.querySelector<HTMLElement>('.home-image');
    if (this.count === 1)
      formLength.style.height = 130 + 'vh';
    else if (this.count === 2)
      formLength.style.height = 150 + 'vh';
    if (this.count === 3)
      formLength.style.height = 170 + 'vh';

    if (this.count >= 4)
      return;
    const guests = this.form.get('guests') as FormArray;
    guests.push(this.createForm());
  }

  get f() {
    return this.form.controls;
  }

  get childs() {
    return this.form.get("guests") as FormArray;
  }

  showForm() {
    this.formIsShown = true;
    // const letter = document.querySelector<HTMLElement>('.home-letter');
    // letter.style.display = "none"
    // const home = document.querySelector<HTMLElement>('.formTab');
    // home.style.display = "block"
  }

  reset() {
    this.form.reset();
    this.formSubmitted = false;
  }

  hideForm() {
    this.formIsShown = false;
    // const form = document.querySelector<HTMLElement>('.formTab');
    // form.style.display = "none";
    // const letter = document.querySelector<HTMLElement>('.home-letter');
    // letter.style.display = "block";
  }

  onSubmit() {

    this.form.get('guests').value.forEach(
      element => {
        element.checkIn = this.form.get('checkIn').value;
        element.checkOut = this.form.get('checkOut').value;
        element.message = this.form.get('message').value;
      });

    this.checkInDate = this.form.get('checkIn').value;
    this.checkOutDate = this.form.get('checkOut').value;
    if (this.checkOutDate > this.checkInDate)
      this.incorrectDates = false;



    //const home = document.querySelector<HTMLElement>('.home-image');
    //home.style.height = 130 + "vh";
    this.form.get('isGuest').setValue(0);


    this.formSubmitted = true;
    this.formError = false;
    if (!this.form.valid) {
      console.log(this.form.validator);
      return;
    }
    this.loading = true;
    this.dataService.addEntry(this.form.value).subscribe(
      (response: any) => {
        this.loading = false;
        if (response.status) {
          this.formDone = true;

          setTimeout(
            () => {
              this.hideForm();
              this.formDone = false;

            }, 2000
          )
          this.reset();
        }
      },
      (error) => {
        this.loading = false;
      }
    );
  }
}
